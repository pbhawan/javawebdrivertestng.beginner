package Assignment6;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

public class Assertion {
	
	static WebDriver  ChormeDriver;
	
@BeforeClass
	public static void BeforeClass() {
	System.setProperty("webdriver.chrome.driver",
			"D:\\Documents\\Documents\\TestNG Assignment\\javawebdrivertestng.beginner\\MavanProjectTesting-Q4\\ExtFiles\\chromedriver.exe");
	ChormeDriver= new ChromeDriver();
	ChormeDriver.get("http://automationpractice.com/index.php");

		}
	
	
	
	
	@Test
	public void AssertionFor3and5areNotEqual() {
		try {
			Assert.assertNotEquals(3, 5);
			System.out.println("Both are not equal");
		} catch (Exception e) {
			System.out.println("Both are equal");

		}
	}

	@Test
	public void AssertionFor3isGreaterThen5() {
		try {
			Assert.assertFalse(3 > 5, "3 is not greater then 5");
			System.out.println("3 is not greater then 5");
		} catch (Exception e) {
			System.out.println(e);

		}
	}

	@Test
	public void Palindrome() {
		int n = 16461, r, sum = 0, temp;
		temp = n;
		while (n > 0) {
			r = n % 10;
			sum = (sum * 10) + r;
			n = n / 10;
		}

		try {
			Assert.assertEquals(temp, sum, "Number is palidrome");
			System.out.println("Number is palidrome");
		} catch (Exception e) {
			System.out.println("Number is not palidrome");

		}
	}

	@Test
	public void VerifyHomePageIsRenderdAfterClickingOnYourLogo() {
		

		ChormeDriver.findElement(By.xpath("//*[@id='block_top_menu']/ul/li[2]/a")).click();
		ChormeDriver.findElement(By.xpath("//*[@id='header_logo']/a/img")).click();
		String URL = ChormeDriver.getCurrentUrl();
		Assert.assertEquals(URL, "http://automationpractice.com/index.php", "Home Page  not Launched");
	}

	@Test
	public void VerifyContactUsPage() {

		
		ChormeDriver.findElement(By.xpath("//*[@id='contact-link']/a")).click();
		String ContactUs_Text = ChormeDriver.findElement(By.xpath("//*[@id='center_column']/h1")).getText();
		Assert.assertEquals(ContactUs_Text, "CUSTOMER SERVICE - CONTACT US", "Contact Us Page not renderd");

	}

	@Test
	public void VerifyNoResultForSerachKeyword() {
		
		
         ChormeDriver.findElement(By.xpath("//*[@id='search_query_top']")).sendKeys("ABCDEFGH");
         ChormeDriver.findElement(By.xpath("//*[@id='searchbox']/button")).click();
         String Search_Result = ChormeDriver.findElement(By.xpath("//*[@id='center_column']/p")).getText();
         Assert.assertEquals(Search_Result, "No results were found for your search \"ABCDEFGH\"", " Result found");
	}
	@AfterClass
	public static void AfterClass() {
		ChormeDriver.close();
		 // System.out.println("Top 10 gainer ");

		}
	
}
