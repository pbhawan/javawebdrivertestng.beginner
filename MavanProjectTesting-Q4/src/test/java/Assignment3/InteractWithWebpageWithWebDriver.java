package Assignment3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class InteractWithWebpageWithWebDriver {
	static WebDriver  driver;
	
@BeforeClass
	public static void BeforeClass() {
	System.setProperty("webdriver.chrome.driver",
			"D:\\Documents\\Documents\\TestNG Assignment\\javawebdrivertestng.beginner\\MavanProjectTesting-Q4\\ExtFiles\\chromedriver.exe");
	driver= new ChromeDriver();
	driver.get("http://www.seleniumframework.com/Practiceform/");

		}

	@Test
	public void GoToBackPage() {
		
		driver.findElement(By.xpath("//li[contains(@class,'menu-item-home')]")).click();
		driver.navigate().back();

	}

	@Test
	public void DisplayTitleofTheLink() {
		
		String s = driver.findElement(By.xpath("//a[text()='This is a link']")).getAttribute("Title").toString();
		System.out.print(s);

	}

	@Test
	public void AfterEnteringEmailAddressClickOnSubscribeButton() {
	
		driver.findElement(By.xpath("//input[@name='email']")).sendKeys("pratibhabhawan23@gmail.com");
		driver.findElement(By.xpath("//input[@value='Subscribe']")).click();

	}
	@Test
	public void EnterContactUsForm() {
	
		String EmailName = driver.findElement(By.xpath("//span[@class='form-name']/input[@name='name']")).getAttribute("name").toString();
		driver.findElement(By.xpath("//span[@class='form-name']/input[@name='name']")).sendKeys(EmailName);
		driver.findElement(By.xpath("//span[@class='form-mail']/input[@name='email']")).sendKeys("pratibhab@gmail.com");
		driver.findElement(By.xpath("//span[@class='form-telephone']/input[@name='telephone']")).sendKeys("941456633");
		driver.findElement(By.xpath("//span[@class='form-country']/input[@name='country']")).sendKeys("Chin");
		driver.findElement(By.xpath("//span[@class='form-company']/input[@name='company']")).sendKeys("Metacube");
		driver.findElement(By.xpath("//span[@class='form-message']/textarea[@name='message']")).sendKeys("Metacube");
		driver.findElement(By.xpath("//a[contains(@class,'btn-submit')]")).click();
	}
	 @AfterClass
		public static void AfterClass() {
		 driver.close();
			 // System.out.println("Top 10 gainer ");

			}
}
