package Assignment4;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.ArrayList;

public class LocatorAndxpath {
	static WebDriver  driver;
	
	@BeforeClass
		public static void BeforeClass() {
		System.setProperty("webdriver.chrome.driver",
				"D:\\Documents\\Documents\\TestNG Assignment\\javawebdrivertestng.beginner\\MavanProjectTesting-Q4\\ExtFiles\\chromedriver.exe");
		driver= new ChromeDriver();
		driver.get("http://www.seleniumframework.com/Practiceform/");

			}
	@Test
	public void TotalInputBoxesAvailableOnForm() {

	
		java.util.List<WebElement> textboxes = driver.findElements(By.xpath("//input[@type='text']"));

		System.out.println(textboxes.size());
	}

	@Test
	public void ColorCodeoftheBoxWithTitleChangeColor() {

	
		WebElement ColorCode = driver.findElement(By.xpath("//button[@id='colorVar']"));

		System.out.println("Color of Change Color Text: = " + ColorCode.getCssValue("color"));
	}

	@Test
	public void CountTotalNumberofButtonAvailableOnTheForm() {

	
		java.util.List<WebElement> buttons = driver.findElements(By.xpath("//button"));

		System.out.println("Number of Buttons Available on form: = " + buttons.size());
	}

	@Test
	public void WriteTextof6thAndLastButton() {

	
		ArrayList<WebElement> buttons = (ArrayList<WebElement>) driver.findElements(By.xpath("//button"));
		java.util.Iterator<WebElement> itr = buttons.iterator();
		boolean s = itr.hasNext();
		int i = 0;
		while (s) {
			itr.next();
			i++;
			String f;

			if (i == 5) {
				f = itr.next().getText().toString();
				System.out.println(f);
			}
			if (i == 6) {
				f = itr.next().getText().toString();
				System.out.println(f);
				s = false;
			}
		}

	}
	 @AfterClass
		public static void AfterClass() {
		 driver.close();
			 // System.out.println("Top 10 gainer ");

			}

	
}
