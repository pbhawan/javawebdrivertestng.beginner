package Assignment5;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class HandlingDifferentWebobjectswithWebDriver {
static WebDriver  driver;

	@Test
	public void ClickOnLinkElement8() {
		System.setProperty("webdriver.chrome.driver",
				"D:\\Documents\\Documents\\TestNG Assignment\\javawebdrivertestng.beginner\\MavanProjectTesting-Q4\\ExtFiles\\chromedriver.exe");
		 driver = new ChromeDriver();
		driver.get("http://www.seleniumframework.com/Practiceform/");
		driver.manage().timeouts().implicitlyWait(45000, TimeUnit.MILLISECONDS);	
		WebElement Element8 = driver.findElement(By.xpath("//a[contains(text(),'Element8')]"));
		Element8.click();
	}

	@Test
	public void LaunchApplicatonAndFillFields() {
		//site not found
		System.setProperty("webdriver.chrome.driver",
				"D:\\Documents\\Documents\\TestNG Assignment\\javawebdrivertestng.beginner\\MavanProjectTesting-Q4\\ExtFiles\\chromedriver.exe");
		 driver = new ChromeDriver();
		 driver.get("http://toolsqa.wpengine.com/automation-practice-form/");
		 driver.findElement(By.name("firstname")).sendKeys("Pratibha");
		 driver.findElement(By.name("lastname")).sendKeys("Bhawan");
		 driver.findElement(By.xpath("//input[@id='sex-1']")).click();
		 driver.findElement(By.xpath("//input[@id='exp-5']")).click();
		 driver.findElement(By.xpath("//input[@id='datepicker']")).sendKeys("16/12/2018");
		 driver.findElement(By.xpath("//input[@id='profession-1']")).click();
		 driver.findElement(By.xpath("//input[@id='tool-2']")).click();
		 driver.findElement(By.id("continents")).findElement(By.xpath(".//option[contains(text(),'South America')]")).click();
		 driver.findElement(By.id("selenium_commands")).findElement(By.xpath(".//option[contains(text(),'Switch Commands')]")).click();
		 driver.findElement(By.xpath("//Button[@id='submit']")).click();
	}

	@Test
	public void Top10Gainer() {
		System.setProperty("webdriver.chrome.driver",
				"D:\\Documents\\Documents\\TestNG Assignment\\javawebdrivertestng.beginner\\MavanProjectTesting-Q4\\ExtFiles\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://money.rediff.com/losers/bse/daily");
		driver.findElement(By.xpath("//a[contains(@href, 'money.rediff.com/gainers/bse')]")).click();
          int iColumnCount = driver.findElements(By.xpath("//*[@id='leftcontainer']/table/thead/tr/th")).size();
          java.util.List<WebElement> Rows = driver.findElements(By.xpath("//*[@id='leftcontainer']/table/tbody/tr/td[1]"));
          java.util.Iterator<WebElement> itr = Rows.iterator();
              for (int i = 0; i <10; i++)
          {
              String s = itr.next().getText();
         
              System.out.println("Top 10 gainer = " + s);
             
          }
          }
	

	@Test
	public void Top10Loser() {
		System.setProperty("webdriver.chrome.driver",
				"D:\\Documents\\Documents\\TestNG Assignment\\javawebdrivertestng.beginner\\MavanProjectTesting-Q4\\ExtFiles\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://money.rediff.com/losers/bse/daily");
		driver.findElement(By.xpath("//a[contains(@href, 'money.rediff.com/losers/bse/weekly')]")).click();
         int iColumnCount = driver.findElements(By.xpath("//*[@id='leftcontainer']/table/thead/tr/th")).size();
         java.util.List<WebElement> Rows = driver.findElements(By.xpath("//*[@id='leftcontainer']/table/tbody/tr/td[1]"));
         java.util.Iterator<WebElement> itr = Rows.iterator();
             for (int i = 0; i <10; i++)
         {
             String s = itr.next().getText();
        
             System.out.println("Top 10 gainer = " + s);
            
         }
        
	}
	 @AfterClass
		public static void AfterClass() {
		 driver.close();
			 // System.out.println("Top 10 gainer ");

			}

	
}
