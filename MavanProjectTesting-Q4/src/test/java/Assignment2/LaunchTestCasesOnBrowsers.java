package Assignment2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.AfterClass;

import org.testng.annotations.Test;

public class LaunchTestCasesOnBrowsers {
	
	static WebDriver  driver;

	 @Test
	  public void LaunchTestCasesOnChromeBrowser()
	 {
		 System.setProperty("webdriver.chrome.driver", "D:\\Documents\\Documents\\TestNG Assignment\\javawebdrivertestng.beginner\\MavanProjectTesting-Q4\\ExtFiles\\chromedriver.exe");
		 
			WebDriver driver=new ChromeDriver();
			driver.get("https://www.google.com");
			 driver.close();
	  }
	 @Test
	  public void LaunchTestCasesOngeckoBrowser()
	 {
		 System.setProperty("webdriver.gecko.driver", "D:\\Documents\\Documents\\TestNG Assignment\\javawebdrivertestng.beginner\\MavanProjectTesting-Q4\\ExtFiles\\geckodriver.exe");
			WebDriver driver = new FirefoxDriver();
			driver.get("https://www.google.com");
	 	
			driver.quit();
	  }
	 @Test
	  public void LaunchTestCasesOnIEBrowser()
	 {
		
			 String service = "D:\\Documents\\Documents\\TestNG Assignment\\javawebdrivertestng.beginner\\MavanProjectTesting-Q4\\ExtFiles\\IEDriverServer.exe";
			 System.setProperty("webdriver.ie.driver", service);
	 
			 InternetExplorerDriver  driver = new InternetExplorerDriver();
			 driver.get("https://www.google.com");
			 driver.close();
	  }
	 
	 @Test
	  public void LaunchTestCasesOnSafariBrowser()
	  {
	  String service = "D:\\Documents\\Documents\\TestNG Assignment\\MavanProjectTesting-Q4\\ExtFiles\\IEDriverServer.exe";
		 System.setProperty("webdriver.safari.driver", service);	
		  WebDriver driver = new SafariDriver();  	            
        driver.navigate().to("http://www.google.com/");  
        driver.close();
              
      
	  }
	
}
